2020-09-20 10:22:53
Full thread dump Java HotSpot(TM) 64-Bit Server VM (25.221-b11 mixed mode):

"AliveLockThread.CRP0-OAQCPU-APPS" #503 prio=6 os_prio=0 tid=0x0000000028709800 nid=0x4b28 waiting for monitor entry [0x0000000062f8f000]
   java.lang.Thread.State: BLOCKED (on object monitor)
	at oracle.jdbc.driver.PhysicalConnection.isClosed(PhysicalConnection.java:2573)
	- waiting to lock <0x000000008c967260> (a oracle.jdbc.driver.T2CConnection)
	at oracle.jdbc.driver.T2CConnection.isClosed(T2CConnection.java:80)
	at oracle.jdbc.proxy.oracle$1dbtools$1raptor$1proxy$1driver$1oracle$1RaptorProxyOJDBCConnection$2oracle$1jdbc$1internal$1OracleConnection$$$Proxy.isClosed(Unknown Source)
	at oracle.javatools.db.AbstractDatabase$AliveTester.run(AbstractDatabase.java:487)
	- locked <0x000000008c972820> (a oracle.jdbc.proxy.oracle$1dbtools$1raptor$1proxy$1driver$1oracle$1RaptorProxyOJDBCConnection$2oracle$1jdbc$1internal$1OracleConnection$$$Proxy)
	at java.lang.Thread.run(Thread.java:748)

"QueryThread" #502 prio=6 os_prio=0 tid=0x000000001d809000 nid=0x4a80 runnable [0x0000000062e8e000]
   java.lang.Thread.State: RUNNABLE
	at oracle.jdbc.driver.T2CStatement.t2cParseExecuteDescribe(Native Method)
	at oracle.jdbc.driver.T2CPreparedStatement.executeForDescribe(T2CPreparedStatement.java:687)
	at oracle.jdbc.driver.OracleStatement.executeMaybeDescribe(OracleStatement.java:983)
	at oracle.jdbc.driver.OracleStatement.doExecuteWithTimeout(OracleStatement.java:1168)
	at oracle.jdbc.driver.OraclePreparedStatement.executeInternal(OraclePreparedStatement.java:3666)
	at oracle.jdbc.driver.OraclePreparedStatement.executeQuery(OraclePreparedStatement.java:3713)
	- locked <0x000000008c967260> (a oracle.jdbc.driver.T2CConnection)
	at oracle.jdbc.driver.OraclePreparedStatementWrapper.executeQuery(OraclePreparedStatementWrapper.java:1167)
	at oracle.jdbc.proxy.oracle$1dbtools$1raptor$1proxy$1driver$1oracle$1RaptorProxyOJDBCStatement$2oracle$1jdbc$1internal$1OraclePreparedStatement$$$Proxy.executeQuery(Unknown Source)
	at oracle.dbtools.raptor.phighlight.Query$QueryThread.run(Query.java:124)

"Background Parser#5" #481 prio=6 os_prio=0 tid=0x00000000242d2800 nid=0x2f78 waiting on condition [0x000000006118f000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.BackgroundParser$1.construct(BackgroundParser.java:160)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"SwingWorker-pool-8-thread-2" #478 daemon prio=5 os_prio=0 tid=0x000000002870b000 nid=0x1094 waiting on condition [0x000000006078e000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x00000000edeac270> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

"SwingWorker-pool-8-thread-1" #469 daemon prio=5 os_prio=0 tid=0x000000001d801000 nid=0x1808 waiting on condition [0x000000005fe8f000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x00000000edeac270> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

"Background Parser#4" #453 prio=6 os_prio=0 tid=0x00000000242d2000 nid=0x4a18 waiting on condition [0x000000005ee8f000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.BackgroundParser$1.construct(BackgroundParser.java:160)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"RaptorTaskThread10" #334 prio=5 os_prio=0 tid=0x000000001d807800 nid=0x3b00 waiting on condition [0x0000000053f8f000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x0000000081e51648> (a java.util.concurrent.SynchronousQueue$TransferStack)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.SynchronousQueue$TransferStack.awaitFulfill(SynchronousQueue.java:458)
	at java.util.concurrent.SynchronousQueue$TransferStack.transfer(SynchronousQueue.java:362)
	at java.util.concurrent.SynchronousQueue.take(SynchronousQueue.java:924)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

"Background Parser#3" #295 prio=6 os_prio=0 tid=0x000000001d806000 nid=0x49b0 waiting on condition [0x000000005118f000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.BackgroundParser$1.construct(BackgroundParser.java:160)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"Persistence Auto Flusher" #258 daemon prio=5 os_prio=0 tid=0x000000001d7ff800 nid=0x2168 in Object.wait() [0x000000004dc8e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.util.TimerThread.mainLoop(Timer.java:526)
	- locked <0x000000008bd4f9f0> (a java.util.TaskQueue)
	at java.util.TimerThread.run(Timer.java:505)

"Background Parser#2" #175 prio=6 os_prio=0 tid=0x000000001c6f2000 nid=0x3160 waiting on condition [0x0000000046f8e000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.BackgroundParser$1.construct(BackgroundParser.java:160)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"Image Animator 0" #136 daemon prio=3 os_prio=-1 tid=0x0000000028711000 nid=0x3b74 waiting on condition [0x000000004318e000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at sun.awt.image.GifFrame.dispose(GifImageDecoder.java:670)
	at sun.awt.image.GifImageDecoder.readImage(GifImageDecoder.java:458)
	at sun.awt.image.GifImageDecoder.produceImage(GifImageDecoder.java:212)
	at sun.awt.image.InputStreamImageSource.doFetch(InputStreamImageSource.java:269)
	at sun.awt.image.ImageFetcher.fetchloop(ImageFetcher.java:205)
	at sun.awt.image.ImageFetcher.run(ImageFetcher.java:169)

"TextBufferScavenger" #124 prio=6 os_prio=0 tid=0x00000000242cc800 nid=0xd28 in Object.wait() [0x000000004258f000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x00000000888d9f78> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
	at oracle.ide.model.FacadeBufferReference$PollingThread.run(FacadeBufferReference.java:145)

"oracle.jdbc.driver.BlockSource.ThreadedCachingBlockSource.BlockReleaser" #113 daemon prio=4 os_prio=-1 tid=0x0000000028705000 nid=0x2d00 in Object.wait() [0x0000000041a8f000]
   java.lang.Thread.State: TIMED_WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at oracle.jdbc.driver.BlockSource$ThreadedCachingBlockSource$BlockReleaser.run(BlockSource.java:331)
	- locked <0x0000000088bcf1e0> (a oracle.jdbc.driver.BlockSource$ThreadedCachingBlockSource$BlockReleaser)

"Timer-3" #112 daemon prio=5 os_prio=0 tid=0x000000002870c000 nid=0x38b0 in Object.wait() [0x000000004198e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.util.TimerThread.mainLoop(Timer.java:526)
	- locked <0x00000000887de520> (a java.util.TaskQueue)
	at java.util.TimerThread.run(Timer.java:505)

"pool-7-thread-1" #108 prio=5 os_prio=0 tid=0x0000000028702800 nid=0x3934 waiting on condition [0x000000004158f000]
   java.lang.Thread.State: TIMED_WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x0000000088e62318> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.parkNanos(LockSupport.java:215)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2078)
	at java.util.concurrent.ScheduledThreadPoolExecutor$DelayedWorkQueue.take(ScheduledThreadPoolExecutor.java:1093)
	at java.util.concurrent.ScheduledThreadPoolExecutor$DelayedWorkQueue.take(ScheduledThreadPoolExecutor.java:809)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

"Arbori Background Parser#1" #99 prio=5 os_prio=0 tid=0x0000000028708000 nid=0xa18 waiting on condition [0x0000000040c8f000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.structure.arbori.BackgroundParser$1.construct(BackgroundParser.java:147)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"Background Parser#0" #98 prio=5 os_prio=0 tid=0x000000001c42d800 nid=0x1070 waiting on condition [0x0000000040b8e000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.BackgroundParser$1.construct(BackgroundParser.java:160)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"Arbori Background Parser#0" #97 prio=5 os_prio=0 tid=0x00000000242cd800 nid=0x4698 waiting on condition [0x0000000040a8f000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at oracle.dbtools.raptor.plsql.structure.arbori.BackgroundParser$1.construct(BackgroundParser.java:147)
	at oracle.dbtools.raptor.utils.NamedSwingWorker$2.run(NamedSwingWorker.java:115)
	at java.lang.Thread.run(Thread.java:748)

"Disposer" #68 daemon prio=10 os_prio=2 tid=0x000000001c430000 nid=0x1228 in Object.wait() [0x0000000035aff000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x0000000085ed35f0> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
	at com.sun.webkit.Disposer.run(Disposer.java:122)
	at java.lang.Thread.run(Thread.java:748)

"Prism Font Disposer" #67 daemon prio=10 os_prio=2 tid=0x000000001c42a000 nid=0x30d0 in Object.wait() [0x00000000351ff000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x00000000853da448> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
	at com.sun.javafx.font.Disposer.run(Disposer.java:93)
	at java.lang.Thread.run(Thread.java:748)

"Thread-22" #66 daemon prio=5 os_prio=0 tid=0x000000001c427800 nid=0x3a0 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"status-0" #65 prio=2 os_prio=-2 tid=0x000000001c42b800 nid=0x2eb0 waiting on condition [0x00000000337fe000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x0000000085a8cdb0> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.DelayQueue.take(DelayQueue.java:211)
	at oracle.ide.status.StatusExecutor$StatusQueue.take(StatusExecutor.java:338)
	at oracle.ide.status.StatusExecutor$StatusQueue.take(StatusExecutor.java:300)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

"JavaFX Application Thread" #64 daemon prio=5 os_prio=0 tid=0x000000001c42c000 nid=0x4670 runnable [0x00000000336ff000]
   java.lang.Thread.State: RUNNABLE
	at com.sun.glass.ui.win.WinApplication._runLoop(Native Method)
	at com.sun.glass.ui.win.WinApplication.lambda$null$3(WinApplication.java:177)
	at com.sun.glass.ui.win.WinApplication$$Lambda$144/2003669685.run(Unknown Source)
	at java.lang.Thread.run(Thread.java:748)

"Thread-21" #63 daemon prio=5 os_prio=0 tid=0x000000001c42d000 nid=0x2494 waiting on condition [0x00000000335fe000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x0000000085a8b598> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.LinkedBlockingDeque.takeFirst(LinkedBlockingDeque.java:492)
	at com.sun.glass.ui.InvokeLaterDispatcher.run(InvokeLaterDispatcher.java:108)

"QuantumRenderer-0" #61 daemon prio=5 os_prio=0 tid=0x000000001c423800 nid=0x3d0 waiting on condition [0x00000000334fe000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x000000008537f220> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at com.sun.javafx.tk.quantum.QuantumRenderer$PipelineRunnable.run(QuantumRenderer.java:125)
	at java.lang.Thread.run(Thread.java:748)

"WeakDataReference polling" #59 prio=1 os_prio=-2 tid=0x000000001c425000 nid=0x3200 in Object.wait() [0x0000000032eff000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x0000000085b63d28> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
	at oracle.ide.util.WeakDataReference$Cleaner.run(WeakDataReference.java:88)
	at java.lang.Thread.run(Thread.java:748)

"Swing-Shell" #58 daemon prio=6 os_prio=0 tid=0x000000001c42a800 nid=0x2db4 waiting on condition [0x0000000031dbf000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x0000000085a8c060> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at sun.awt.shell.Win32ShellFolderManager2$ComInvoker$3.run(Win32ShellFolderManager2.java:556)
	at java.lang.Thread.run(Thread.java:748)

"InterruptTimer" #55 daemon prio=5 os_prio=0 tid=0x000000001c427000 nid=0x42bc in Object.wait() [0x00000000319bf000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.util.TimerThread.mainLoop(Timer.java:526)
	- locked <0x00000000859d0550> (a java.util.TaskQueue)
	at java.util.TimerThread.run(Timer.java:505)

"IconOverlayTracker Timer: null-jdbcNodeInfoType" #51 prio=5 os_prio=0 tid=0x000000001c426000 nid=0x27dc in Object.wait() [0x00000000313bf000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.util.TimerThread.mainLoop(Timer.java:526)
	- locked <0x000000008608b440> (a java.util.TaskQueue)
	at java.util.TimerThread.run(Timer.java:505)

"InnocuousThread-1" #47 daemon prio=5 os_prio=0 tid=0x00000000242d1000 nid=0x15c0 runnable [0x0000000030bbf000]
   java.lang.Thread.State: RUNNABLE
	at sun.nio.ch.Iocp.getQueuedCompletionStatus(Native Method)
	at sun.nio.ch.Iocp.access$300(Iocp.java:46)
	at sun.nio.ch.Iocp$EventHandlerTask.run(Iocp.java:333)
	at java.lang.Thread.run(Thread.java:748)
	at sun.misc.InnocuousThread.run(InnocuousThread.java:106)

"sshd-SshClient[7bbf2c26]-timer-thread-1" #46 daemon prio=5 os_prio=0 tid=0x00000000242d4000 nid=0x4694 waiting on condition [0x00000000308bf000]
   java.lang.Thread.State: TIMED_WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x00000000846391a0> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.parkNanos(LockSupport.java:215)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2078)
	at java.util.concurrent.ScheduledThreadPoolExecutor$DelayedWorkQueue.take(ScheduledThreadPoolExecutor.java:1093)
	at java.util.concurrent.ScheduledThreadPoolExecutor$DelayedWorkQueue.take(ScheduledThreadPoolExecutor.java:809)
	at java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1074)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

"WaitCursor-Timer" #45 prio=6 os_prio=0 tid=0x00000000242d0800 nid=0x42dc in Object.wait() [0x00000000305bf000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.util.TimerThread.mainLoop(Timer.java:526)
	- locked <0x0000000085abb780> (a java.util.TaskQueue)
	at java.util.TimerThread.run(Timer.java:505)

"Thread-10" #41 daemon prio=5 os_prio=0 tid=0x00000000242c5000 nid=0x4634 in Object.wait() [0x000000002fabe000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.util.prefs.AbstractPreferences$EventDispatchThread.run(AbstractPreferences.java:1482)
	- locked <0x0000000080527768> (a java.util.LinkedList)

"ChangeSetService" #39 daemon prio=1 os_prio=-2 tid=0x00000000242c6000 nid=0x4418 in Object.wait() [0x000000002f6be000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at oracle.jdevimpl.vcs.changeset.ChangeSetService.awaitEvents(ChangeSetService.java:131)
	- locked <0x0000000083bfbf90> (a oracle.jdevimpl.vcs.changeset.ChangeSetService)
	at oracle.jdevimpl.vcs.changeset.ChangeSetService.eventLoop(ChangeSetService.java:152)
	at oracle.jdevimpl.vcs.changeset.ChangeSetService.access$000(ChangeSetService.java:60)
	at oracle.jdevimpl.vcs.changeset.ChangeSetService$1.run(ChangeSetService.java:99)
	at java.lang.Thread.run(Thread.java:748)

"Scheduler" #38 daemon prio=5 os_prio=0 tid=0x00000000242c9000 nid=0x44dc in Object.wait() [0x000000002dfaf000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at oracle.dbtools.raptor.backgroundTask.TaskLinkedList.takeNextTask(TaskLinkedList.java:47)
	- locked <0x0000000081e19bc0> (a oracle.dbtools.raptor.backgroundTask.TaskLinkedList)
	at oracle.dbtools.raptor.backgroundTask.RaptorTaskManager$SchedulerThread.run(RaptorTaskManager.java:548)

"Log Poller" #37 prio=1 os_prio=-2 tid=0x00000000242c9800 nid=0xe34 waiting on condition [0x000000002d80e000]
   java.lang.Thread.State: WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x0000000081ad33e8> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.park(LockSupport.java:175)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.await(AbstractQueuedSynchronizer.java:2039)
	at java.util.concurrent.LinkedBlockingQueue.take(LinkedBlockingQueue.java:442)
	at oracle.ide.log.QueuedLoggingHandler.take(QueuedLoggingHandler.java:60)
	at oracle.ideimpl.log.TabbedLogManager$4.run(TabbedLogManager.java:333)
	at java.lang.Thread.run(Thread.java:748)

"TimedCache-Timer" #33 daemon prio=5 os_prio=0 tid=0x000000001c6ed800 nid=0x24a4 in Object.wait() [0x000000002a6af000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	- waiting on <0x0000000080fdfd68> (a java.util.TaskQueue)
	at java.lang.Object.wait(Object.java:502)
	at java.util.TimerThread.mainLoop(Timer.java:526)
	- locked <0x0000000080fdfd68> (a java.util.TaskQueue)
	at java.util.TimerThread.run(Timer.java:505)

"Framework Event Dispatcher" #29 daemon prio=6 os_prio=0 tid=0x000000001c6f0800 nid=0x39b8 in Object.wait() [0x00000000265fe000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at org.eclipse.osgi.framework.eventmgr.EventManager$EventThread.getNextEvent(EventManager.java:400)
	- locked <0x0000000080938c60> (a org.eclipse.osgi.framework.eventmgr.EventManager$EventThread)
	at org.eclipse.osgi.framework.eventmgr.EventManager$EventThread.run(EventManager.java:336)

"State Data Manager" #27 daemon prio=5 os_prio=0 tid=0x000000001c6f3000 nid=0xd44 waiting on condition [0x000000002522f000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
	at java.lang.Thread.sleep(Native Method)
	at org.eclipse.osgi.internal.baseadaptor.StateManager.run(StateManager.java:297)
	at java.lang.Thread.run(Thread.java:748)

"Framework Active Thread" #26 prio=5 os_prio=0 tid=0x000000001c6ef000 nid=0x246c in Object.wait() [0x000000002512e000]
   java.lang.Thread.State: TIMED_WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at org.eclipse.osgi.framework.internal.core.Framework.run(Framework.java:1870)
	- locked <0x00000000805f2b60> (a org.eclipse.osgi.framework.internal.core.Framework)
	at java.lang.Thread.run(Thread.java:748)

"Active Reference Queue Daemon" #22 daemon prio=1 os_prio=-2 tid=0x000000001c551800 nid=0x30e4 in Object.wait() [0x000000001f81f000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x00000000805d4b48> (a java.lang.ref.ReferenceQueue$Lock)
	at org.openide.util.lookup.implspi.ActiveQueue$Impl.removeSuper(ActiveQueue.java:93)
	at org.openide.util.lookup.implspi.ActiveQueue$Daemon.run(ActiveQueue.java:131)

"TimerQueue" #20 daemon prio=5 os_prio=0 tid=0x000000001c5aa800 nid=0xa2c waiting on condition [0x000000001ed1e000]
   java.lang.Thread.State: TIMED_WAITING (parking)
	at sun.misc.Unsafe.park(Native Method)
	- parking to wait for  <0x00000000809465c0> (a java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject)
	at java.util.concurrent.locks.LockSupport.parkNanos(LockSupport.java:215)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2078)
	at java.util.concurrent.DelayQueue.take(DelayQueue.java:223)
	at javax.swing.TimerQueue.run(TimerQueue.java:174)
	at java.lang.Thread.run(Thread.java:748)

"AWT-EventQueue-0" #19 prio=6 os_prio=0 tid=0x000000001c2c5800 nid=0x2d6c waiting for monitor entry [0x000000001ce1c000]
   java.lang.Thread.State: BLOCKED (on object monitor)
	at oracle.jdbc.driver.PhysicalConnection.getMetaData(PhysicalConnection.java:2754)
	- waiting to lock <0x000000008c967260> (a oracle.jdbc.driver.T2CConnection)
	at oracle.jdbc.driver.T2CConnection.getMetaData(T2CConnection.java:80)
	at oracle.jdbc.proxy.oracle$1dbtools$1raptor$1proxy$1driver$1oracle$1RaptorProxyOJDBCConnection$2oracle$1jdbc$1internal$1OracleConnection$$$Proxy.getMetaData(Unknown Source)
	at oracle.dbtools.raptor.plsql.PLSQLController.setDebug(PLSQLController.java:333)
	at oracle.dbtools.raptor.plsql.PLSQLController.handleEvent(PLSQLController.java:142)
	at oracle.ideimpl.controller.MetaClassController.handleEvent(MetaClassController.java:54)
	at oracle.ide.controller.IdeAction$ControllerDelegatingController.handleEvent(IdeAction.java:1488)
	at oracle.ide.controller.IdeAction.performAction(IdeAction.java:663)
	at oracle.ide.controller.IdeAction.actionPerformedImpl(IdeAction.java:1159)
	at oracle.ide.controller.IdeAction.actionPerformed(IdeAction.java:618)
	at javax.swing.AbstractButton.fireActionPerformed(AbstractButton.java:2022)
	at oracle.ide.controls.ActionMenuToolButton.access$500(ActionMenuToolButton.java:92)
	at oracle.ide.controls.ActionMenuToolButton$FilterActionEvents.actionPerformed(ActionMenuToolButton.java:486)
	at javax.swing.DefaultButtonModel.fireActionPerformed(DefaultButtonModel.java:402)
	at javax.swing.JToggleButton$ToggleButtonModel.setPressed(JToggleButton.java:308)
	at oracle.ide.controls.ActionMenuToolButton$3.setPressed(ActionMenuToolButton.java:264)
	at javax.swing.plaf.basic.BasicButtonListener.mouseReleased(BasicButtonListener.java:252)
	at java.awt.AWTEventMulticaster.mouseReleased(AWTEventMulticaster.java:290)
	at java.awt.Component.processMouseEvent(Component.java:6539)
	at javax.swing.JComponent.processMouseEvent(JComponent.java:3324)
	at java.awt.Component.processEvent(Component.java:6304)
	at java.awt.Container.processEvent(Container.java:2239)
	at java.awt.Component.dispatchEventImpl(Component.java:4889)
	at java.awt.Container.dispatchEventImpl(Container.java:2297)
	at java.awt.Component.dispatchEvent(Component.java:4711)
	at java.awt.LightweightDispatcher.retargetMouseEvent(Container.java:4904)
	at java.awt.LightweightDispatcher.processMouseEvent(Container.java:4535)
	at java.awt.LightweightDispatcher.dispatchEvent(Container.java:4476)
	at java.awt.Container.dispatchEventImpl(Container.java:2283)
	at java.awt.Window.dispatchEventImpl(Window.java:2746)
	at java.awt.Component.dispatchEvent(Component.java:4711)
	at java.awt.EventQueue.dispatchEventImpl(EventQueue.java:760)
	at java.awt.EventQueue.access$500(EventQueue.java:97)
	at java.awt.EventQueue$3.run(EventQueue.java:709)
	at java.awt.EventQueue$3.run(EventQueue.java:703)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(ProtectionDomain.java:74)
	at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(ProtectionDomain.java:84)
	at java.awt.EventQueue$4.run(EventQueue.java:733)
	at java.awt.EventQueue$4.run(EventQueue.java:731)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.security.ProtectionDomain$JavaSecurityAccessImpl.doIntersectionPrivilege(ProtectionDomain.java:74)
	at java.awt.EventQueue.dispatchEvent(EventQueue.java:730)
	at oracle.javatools.internal.ui.EventQueueWrapper._dispatchEvent(EventQueueWrapper.java:169)
	at oracle.javatools.internal.ui.EventQueueWrapper.dispatchEvent(EventQueueWrapper.java:151)
	at java.awt.EventDispatchThread.pumpOneEventForFilters(EventDispatchThread.java:205)
	at java.awt.EventDispatchThread.pumpEventsForFilter(EventDispatchThread.java:116)
	at java.awt.EventDispatchThread.pumpEventsForHierarchy(EventDispatchThread.java:105)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:101)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:93)
	at java.awt.EventDispatchThread.run(EventDispatchThread.java:82)

"Thread-3" #18 daemon prio=5 os_prio=0 tid=0x000000001c32e800 nid=0x413c runnable [0x000000001cd1f000]
   java.lang.Thread.State: RUNNABLE
	at com.sun.java.accessibility.AccessBridge.runDLL(Native Method)
	at com.sun.java.accessibility.AccessBridge.access$300(AccessBridge.java:57)
	at com.sun.java.accessibility.AccessBridge$dllRunner.run(AccessBridge.java:141)
	at java.lang.Thread.run(Thread.java:748)

"EventQueueMonitor-ComponentEvtDispatch" #17 daemon prio=5 os_prio=0 tid=0x000000001c1a7800 nid=0x4460 in Object.wait() [0x000000001cc1e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at com.sun.java.accessibility.util.ComponentEvtDispatchThread.run(EventQueueMonitor.java:582)
	- locked <0x0000000080695f78> (a java.lang.Object)

"AWT-Windows" #14 daemon prio=6 os_prio=0 tid=0x000000001c17d000 nid=0x41a8 runnable [0x000000001cb1f000]
   java.lang.Thread.State: RUNNABLE
	at sun.awt.windows.WToolkit.eventLoop(Native Method)
	at sun.awt.windows.WToolkit.run(WToolkit.java:315)
	at java.lang.Thread.run(Thread.java:748)

"AWT-Shutdown" #13 prio=5 os_prio=0 tid=0x000000001c178800 nid=0x2eac in Object.wait() [0x000000001ca1e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at sun.awt.AWTAutoShutdown.run(AWTAutoShutdown.java:295)
	- locked <0x0000000080517478> (a java.lang.Object)
	at java.lang.Thread.run(Thread.java:748)

"Java2D Disposer" #12 daemon prio=10 os_prio=2 tid=0x000000001c115800 nid=0x24fc in Object.wait() [0x000000001c91e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x00000000805b6938> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
	at sun.java2d.Disposer.run(Disposer.java:148)
	at java.lang.Thread.run(Thread.java:748)

"Service Thread" #10 daemon prio=9 os_prio=0 tid=0x000000001a403000 nid=0x4304 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"C1 CompilerThread3" #9 daemon prio=9 os_prio=2 tid=0x000000001a3cc000 nid=0x3bc0 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"C2 CompilerThread2" #8 daemon prio=9 os_prio=2 tid=0x000000001a3c9000 nid=0x2520 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"C2 CompilerThread1" #7 daemon prio=9 os_prio=2 tid=0x000000001a3bc000 nid=0x1a2c waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"C2 CompilerThread0" #6 daemon prio=9 os_prio=2 tid=0x000000001a3b4800 nid=0x23b4 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"Attach Listener" #5 daemon prio=5 os_prio=2 tid=0x000000001a3b3800 nid=0x35c0 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"Signal Dispatcher" #4 daemon prio=9 os_prio=2 tid=0x000000001a3b2000 nid=0x3b2c runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"Finalizer" #3 daemon prio=8 os_prio=1 tid=0x0000000018cc5800 nid=0x4004 in Object.wait() [0x000000001b71f000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
	- locked <0x00000000806355a0> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
	at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:216)

"Reference Handler" #2 daemon prio=10 os_prio=2 tid=0x000000001a3a3000 nid=0x920 in Object.wait() [0x000000001b61e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	at java.lang.Object.wait(Object.java:502)
	at java.lang.ref.Reference.tryHandlePending(Reference.java:191)
	- locked <0x00000000804ede90> (a java.lang.ref.Reference$Lock)
	at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:153)

"main" #1 prio=5 os_prio=0 tid=0x0000000003956800 nid=0x1e20 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

"VM Thread" os_prio=2 tid=0x000000001a382800 nid=0x884 runnable 

"GC task thread#0 (ParallelGC)" os_prio=0 tid=0x000000000396b800 nid=0x4278 runnable 

"GC task thread#1 (ParallelGC)" os_prio=0 tid=0x000000000396d000 nid=0xe00 runnable 

"GC task thread#2 (ParallelGC)" os_prio=0 tid=0x000000000396e800 nid=0x4464 runnable 

"GC task thread#3 (ParallelGC)" os_prio=0 tid=0x0000000003970800 nid=0x4778 runnable 

"GC task thread#4 (ParallelGC)" os_prio=0 tid=0x0000000003973000 nid=0x1c4c runnable 

"GC task thread#5 (ParallelGC)" os_prio=0 tid=0x0000000003974000 nid=0x39e0 runnable 

"GC task thread#6 (ParallelGC)" os_prio=0 tid=0x0000000003977800 nid=0x1954 runnable 

"GC task thread#7 (ParallelGC)" os_prio=0 tid=0x0000000003978800 nid=0x3e5c runnable 

"GC task thread#8 (ParallelGC)" os_prio=0 tid=0x0000000003979800 nid=0x784 runnable 

"GC task thread#9 (ParallelGC)" os_prio=0 tid=0x000000000397b000 nid=0x353c runnable 

"VM Periodic Task Thread" os_prio=2 tid=0x000000001a404000 nid=0x4554 waiting on condition 

JNI global references: 4676

